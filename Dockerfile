FROM debian:testing

# the texlive scheme to use
ARG SCHEME=small

# see https://github.com/overleaf/docker-image/blob/master/Dockerfile

MAINTAINER Matthieu Simonin <matthieu.simonin@inria.fr>


RUN apt-get update -qq && apt-get -y upgrade
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
  wget\
  sudo\
  perl\
  libfontconfig\
  python3\
  python3-pip\
  python3-pygments\
  graphviz

# Install TexLive
RUN apt-get install -y wget
RUN wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
  mkdir /install-tl-unx && \
  tar -xvf install-tl-unx.tar.gz -C /install-tl-unx --strip-components=1

RUN echo "selected_scheme scheme-$SCHEME" > /install-tl-unx/texlive.profile;
RUN echo "option_doc 0" >> /install-tl-unx/texlive.profile;
RUN /install-tl-unx/install-tl -profile /install-tl-unx/texlive.profile
RUN rm -r /install-tl-unx && \
  rm install-tl-unx.tar.gz

ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/texlive/2022/bin/x86_64-linux/
RUN tlmgr install latexmk
RUN tlmgr install texcount
RUN tlmgr install chktex

# Install Aspell
RUN apt-get install -y aspell aspell-en aspell-fr aspell-es

# ghostscript is used for instance when eps are included in source
# it will convert them to pdf
RUN apt-get install -y ghostscript

## Install dot2tex
RUN pip3 install dot2tex
RUN tlmgr install xkeyval
RUN tlmgr install moreverb

# Links CLSI sycntex to its default location
# ------------------------------------------
# NOTE(msimonin) we embed synctex in the worker
# Alternatively there's a way to mount it in the compilation worker using the settings.coffee
# see https://github.com/overleaf/clsi/blob/1ee48d02741cffb4a9413c9a3845c5ee7fcca271/app/js/DockerRunner.js#L318-L325
COPY bin/synctex /opt/synctex
RUN chmod a+x /opt/synctex

# some extra fonts
RUN cp $(kpsewhich -var-value TEXMFSYSVAR)/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf
RUN mkdir -p /usr/share/fonts/truetype/Monaco
RUN wget http://jorrel.googlepages.com/Monaco_Linux.ttf -O /usr/share/fonts/truetype/Monaco/Monaco_Linux.ttf
RUN fc-cache -fsv

# fix locale
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV PYTHONIOENCODING UTF-8
